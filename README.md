# Tp Réalité Virtuelle - les bases
Réalisé avec :
- Unity 2019.4.13f1 (LTS)
- Oculus Rift S

## Sommaire
- [x] Installation de XR Toolkit Interaction.
- [x] Setup de la caméra avec Xr Rig
- [x] Téléportation.
- [x] Mouvement continue.
- [x] Manipulation (Prendre des objets).
