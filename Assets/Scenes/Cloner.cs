﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;
public class Cloner : MonoBehaviour
{
    public InputDeviceCharacteristics ControllerChar;
    private InputDevice input;
    private bool isCollision= false;
    // Start is called before the first frame update
    void Start()
    {
        List<InputDevice> devices = new List<InputDevice>();

        InputDevices.GetDevicesWithCharacteristics(ControllerChar,devices);

        if(devices.Count>0)
        {
            input = devices[0];
        }
    }

    // Update is called once per frame
    void Update()
    {
        input.TryGetFeatureValue(CommonUsages.grip,out float GripValue);
        input.TryGetFeatureValue(CommonUsages.trigger,out float TriggerValue);
        input.TryGetFeatureValue(CommonUsages.primaryButton,out bool pBtnValue);
        input.TryGetFeatureValue(CommonUsages.secondaryButton,out bool sBtnValue);
        input.TryGetFeatureValue(CommonUsages.primary2DAxis,out Vector2 p2DaxisValue);

        if(GripValue> 0.1f)
            Debug.Log("Grip -> "+GripValue);
        
        if(TriggerValue> 0.1f)
            Debug.Log("trigger -> "+TriggerValue);
        
        if(pBtnValue)
            Debug.Log("primary btton pressed ");

        if(sBtnValue)
            Debug.Log("secandary btton pressed ");

        if(p2DaxisValue != Vector2.zero)
            Debug.Log("p2DaxisValue -> "+p2DaxisValue);

        GameObject [] gameObject = GameObject.FindGameObjectsWithTag("Clonable");

        if(pBtnValue && isCollision)
        {
            if(gameObject.Length >  0)
        {
            GameObject newOne = Instantiate(gameObject[0], gameObject[0].transform.position, gameObject[0].transform.rotation);
            newOne.GetComponent<Rigidbody>().useGravity = true;
            newOne.GetComponent<Rigidbody>().isKinematic = false;
        }
        }
        

    }

    void OnTriggerEnter(Collider c)
    {
        if(c.CompareTag("Clonable"))
        {
            isCollision = true;
        }
    }
    void OnTriggerExit(Collider c)
    {
        if(c.CompareTag("Clonable"))
        {
            isCollision = false;
        }
    }
}
